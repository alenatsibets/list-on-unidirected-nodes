public class List {
    private Node first;
    private Node last;
    public List() {
        first = null;
        last = null;
    }
    public boolean isEmpty() {
        return first == null;
    }
    public void pushBack(String val) {
        Node p = new Node(val);
        if (isEmpty()) {
            first = p;
            last = p;
            return;
        }
        last.setNext(p);
        last = p;
    }
    public int size() {
        int count = 0;
        Node current = first;
        while(current != last) {
            count++;
            current = current.getNext();
        }
        count++;
        return count;
    }
    public String get(int index) {
        if (index < 0 || index > size()-1) {
            throw new RuntimeException("Index is invalid.");
        }
        Node current = first;
        for(int i=0; i<index; i++) {
            current = current.getNext();
        }
        return current.getVal();
    }
    public void insert(String val, int index){
        if (index < 0 || index > size()) {
            throw new RuntimeException("Index is invalid.");
        }
        Node p = new Node(val);
        if (index == 0){
            p.setNext(first);
            first = p;
        } else {
            Node current = first;
            for (int i = 0; i < index - 1; i++) {
                current = current.getNext();
            }
            p.setNext(current.getNext());
            current.setNext(p);
        }
    }
    public void delete(int index){
        if (index < 0 || index > size()) {
            throw new RuntimeException("Index is invalid.");
        }
        if (index == 0){
            first = first.getNext();
        } else {
            Node current = first;
            for (int i = 0; i < index - 1; i++) {
                current = current.getNext();
            }
            current.setNext(current.getNext().getNext());
        }
    }
    public void replace(String val, int index){
        if (index < 0 || index > size()) {
            throw new RuntimeException("Index is invalid.");
        }
        Node p = new Node(val);
        if (index == 0){
            p.setNext(first.getNext());
            first = p;
        } else {
            Node current = first;
            for (int i = 0; i < index - 1; i++) {
                current = current.getNext();
            }
            p.setNext(current.getNext().getNext());
            current.setNext(p);
        }
    }
    public void clear(){
        first = null;
        last = null;
    }

    @Override
    public String toString() {
        return "List{" +
                "first=" + first +
                ", last=" + last +
                '}';
    }
}
