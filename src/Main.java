public class Main {
    public static void main(String[] args) {
        List list= new List();
        list.pushBack("one");
        list.pushBack("two");
        list.pushBack("three");
        System.out.println(list);
        System.out.println(list.size());
        list.clear();
        System.out.println(list.isEmpty());
        list.pushBack("one");
        list.pushBack("two");
        list.pushBack("three");
        list.pushBack("four");
        list.pushBack("five");
        list.pushBack("six");
        System.out.println(list.get(4));
        list.insert("something", 1);
        System.out.println(list.size());
        System.out.println(list);
        list.delete(1);
        System.out.println(list.size());
        System.out.println(list);
        list.replace("beginning", 0);
        System.out.println(list);
    }
}