public class Node {
    private final String val;
    private Node next;
    public Node(String val) {
        this.val = val;
        next = null;
    }
    public String getVal() {
        return val;
    }
    public Node getNext() {
        return next;
    }
    public void setNext(Node next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return "Node{" +
                "val='" + val + '\'' +
                ", next=" + next +
                '}';
    }
}
